use halconair;
insert into pasajero values (1,'13488601','CC','Tito Alfredo','Oliveros Gaitan','1979-11-11','calle 12 No 3-12','5789687');
insert into pasajero values (2,'60123654','CC','Martha Lucia','Perez Suarez','1987-06-10','calle 24 No 5-16','5845698');
insert into pasajero values (3,'1090962687','CC','Fernado','Garcia Sanguino','1980-01-30','Avenida 07 No 6-89','5744259');
select * from pasajero;

insert into avion values (1,'Boeing 747','HK-AAA',20,200);
insert into avion values (2,'Boeing 767','HK-AAG',20,180);
insert into avion values (3,'Airbus A340','HK-BAA',25,220);
insert into aeropuerto values (1,'Aeropuerto Internacional Camilo Daza','Colombia','Norte de Santander','Cúcuta','Carretera Aeropuerto, 54001 San José de Cúcuta','123456',75548,723054);
insert into aeropuerto values (2,'Aeropuerto Internacional El Dorado','Colombia','Distrito Especial','Bogota',' Calle 26 No. 103-09, Ac. 26, Fontibon, Bogota','666666',12040,77089);
insert into aeropuerto values (3,'Aeropuerto Benito Salas','Colombia','Huila','Neiva','Carrera 2, 41001 Neiva','777777',25715,751738);

insert into vuelo values (1,1,'2021-09-30','13:15',1,2,10,180);
insert into vuelo values (2,2,'2021-09-29','08:15',2,3,10,200);
insert into vuelo values (3,3,'2021-09-29','17:45',3,2,10,200);

insert into pasaje values (1,'ejecutiva','A-001',250000,1,1);
insert into pasaje values (2,'ejecutiva','A-002',250000,2,1);
insert into pasaje values (3,'ejecutiva','B-001',250000,3,1);
